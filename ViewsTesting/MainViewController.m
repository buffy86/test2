//
//  MainViewController.m
//  ViewsTesting
//
//  Created by moon on 20/12/12.
//  Copyright (c) 2012 moon. All rights reserved.
//

#import "MainViewController.h"

@interface MainViewController ()

@property (nonatomic, strong) IBOutlet UIImageView *background;

@end

@implementation MainViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        
        [self.view addSubview:self.background];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    //123
}


- (IBAction)yellowBackgroundChosen:(id)sender {
//    self.background.image = [UIImage imageNamed:@"1yellow.jpeg"];
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"1yellow.jpg"]];
}

- (IBAction)blueBackgroundChosen:(id)sender {
//    self.background.image = [UIImage imageNamed:@"2blue.jpg"];
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"2blue.jpg"]];
}

- (IBAction)pinkBackgroundChosen:(id)sender {
//    self.background.image = [UIImage imageNamed:@"3pink.jpg"];
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"3pink.jpg"]];
}

@end
